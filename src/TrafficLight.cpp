#include <iostream>
#include <random>
#include "TrafficLight.h"

int generateRandomNumber(int min, int max){
	return rand()%(max-min + 1) + min;
}

/* Implementation of class "MessageQueue" */
template <typename T>
T MessageQueue<T>::receive()
{
  std::unique_lock<std::mutex> uLock(_mutex);
  _cond.wait(uLock, [this] { return !_queue.empty(); }); // pass unique lock to condition variable

  // remove last vector element from queue
  T msg = std::move(_queue.back());
  _queue.pop_back();

   return msg; 
}

template <typename T>
void MessageQueue<T>::send(T &&msg)
{
  std::lock_guard<std::mutex> uLock(_mutex);
  _queue.clear();
  // add vector to queue
  _queue.emplace_back(std::move(msg));
  _cond.notify_one(); // notify client after pushing new Vehicle into vector
}

/* Implementation of class "TrafficLight" */
TrafficLight::TrafficLight()
{
    _currentPhase = TrafficLightPhase::red;
  	_messages = std::make_shared<MessageQueue<TrafficLightPhase>>();
}

void TrafficLight::waitForGreen()
{
  while(true){
     if(_messages->receive() == TrafficLightPhase::green)
       return;
  }
}

TrafficLightPhase TrafficLight::getCurrentPhase()
{
    return _currentPhase;
}

void TrafficLight::setCurrentPhase(TrafficLightPhase currentPhase)
{
    _currentPhase = currentPhase;
}

void TrafficLight::simulate()
{
  threads.emplace_back(std::thread(&TrafficLight::cycleThroughPhases, this));
}

// virtual function which is executed in a thread
void TrafficLight::cycleThroughPhases()
{  
  double cycleDuration = generateRandomNumber(4000,6000); // duration of a single simulation cycle in ms
  std::chrono::time_point<std::chrono::system_clock> lastUpdate;
  // init stop watch
  lastUpdate = std::chrono::system_clock::now();
  
  while(true){
  	// sleep at every iteration to reduce CPU usage
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    // compute time difference to stop watch
    long timeSinceLastUpdate = 		std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - lastUpdate).count();
    	if (timeSinceLastUpdate >= cycleDuration)
        {	
          	TrafficLightPhase currentPhase = getCurrentPhase();
          	_currentPhase = _currentPhase == TrafficLightPhase::red ? TrafficLightPhase::green : TrafficLightPhase::red; //toggle
          
            _messages->send(std::move(currentPhase));
          
          	lastUpdate = std::chrono::system_clock::now();
            cycleDuration = generateRandomNumber(4000,6000);
        }
  }// end of while
}